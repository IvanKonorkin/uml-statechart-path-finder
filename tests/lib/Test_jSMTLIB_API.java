/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package lib;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

import org.smtlib.*;
import org.smtlib.command.*;
import org.smtlib.impl.SMTExpr.*;
import org.smtlib.impl.Sort.Parameter;
import org.smtlib.solvers.*;

public class Test_jSMTLIB_API {
	@Test
	public void TestEmptyInput()
	{
		SMT smt = new SMT();
		String z3_path = smt.readProperties().getProperty("org.smtlib.solver_z3_4_3.exec");
		ISolver solver = new Solver_z3_4_3(smt.smtConfig, z3_path);
		solver.start();
		solver.set_logic("QF_LIA", null);
		assertEquals("sat", solver.check_sat().toString());
		solver.exit();
	}
	
	@Test
	public void TestEquals()
	{
		SMT smt = new SMT();
		String z3_path = smt.readProperties().getProperty("org.smtlib.solver_z3_4_3.exec");
		ISolver solver = new Solver_z3_4_3(smt.smtConfig, z3_path);
		solver.start();
		solver.set_option(new Keyword(":produce-models"), new Symbol("true"));
		solver.set_logic("QF_LIA", null);
		solver.declare_fun(new C_declare_fun(new Symbol("x"), new ArrayList<ISort>(), new Parameter(new Symbol("Int"))));
		solver.assertExpr(new Symbol("(= x 10)"));
		assertEquals("sat", solver.check_sat().toString());
		assertEquals("( ( x 10 ) )", solver.get_value(new Symbol("x")).toString());
		solver.exit();
	}
	
	@Test
	public void TestUnsat()
	{
		SMT smt = new SMT();
		String z3_path = smt.readProperties().getProperty("org.smtlib.solver_z3_4_3.exec");
		ISolver solver = new Solver_z3_4_3(smt.smtConfig, z3_path);
		solver.start();
		solver.set_logic("QF_LIA", null);
		solver.declare_fun(new C_declare_fun(new Symbol("x"), new ArrayList<ISort>(), new Parameter(new Symbol("Int"))));
		solver.assertExpr(new Symbol("(> x 10)"));
		solver.assertExpr(new Symbol("(< x 10)"));
		assertEquals("unsat", solver.check_sat().toString());
		solver.exit();
	}
}
