/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package storage;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import core.State;
import core.Transition;

public class TestEdgeList {
	@Test
	public void testInitState()
	{
		ArrayList<State> states = new ArrayList<State>();
		State init_state;
		states.add(new State("state 1"));
		states.add(init_state = new State("state 2"));
		states.add(new State("state 3"));
		
		EdgeList storage = new EdgeList();
		for (int i = 0; i < states.size(); ++i) storage.addState(states.get(i));
		
		assertEquals(states.get(0), storage.getInitState());
		storage.setInitState(init_state);
		assertEquals(init_state, storage.getInitState());
	}
	
	@Test
	public void testTransitions()
	{
		State state = new State("state id");
		State state_without_transitions = new State("state id");
		State target_state = new State("state id");
		ArrayList<Transition> transitions = new ArrayList<Transition>();
		transitions.add(new Transition("Transition 1", target_state, "cond", "action"));
		transitions.add(new Transition("Transition 2", target_state, "cond", "action"));
		transitions.add(new Transition("Transition 3", target_state, "cond", "action"));
		
		EdgeList storage = new EdgeList();
		storage.addState(state);
		storage.addState(state_without_transitions);
		for (Transition transition : transitions) storage.addTransition(state, transition);
		
		Collection<Transition> transitions_from_storage = storage.getStateTransitions(state);
		assertEquals(transitions_from_storage.size(), transitions.size());
		assertTrue(transitions_from_storage.containsAll(transitions));
		transitions_from_storage = storage.getStateTransitions(state_without_transitions);
		assertTrue(transitions_from_storage == null || transitions_from_storage.size() == 0);
		transitions_from_storage = storage.getStateTransitions(target_state);
		assertTrue(transitions_from_storage == null || transitions_from_storage.size() == 0);
	}
	
	@Test
	public void testGetStateById()
	{
		State state = new State("state id");
		
		EdgeList storage = new EdgeList();
		storage.addState(state);
		
		assertEquals(state, storage.getStateById("state id"));
		assertNull(storage.getStateById("another id"));
	}
}
