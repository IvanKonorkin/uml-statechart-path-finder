/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package parser;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import core.State;
import storage.EdgeList;
import storage.Storage;

public class TestParser {
	@Test
	public void testParser()
	{
		Parser parser = new Parser("tests/parser/files/testParser.scxml");
		Storage storage = new EdgeList();
		parser.parseDocument(storage);
		
		State ready_state = storage.getStateById("ready");
		State stopped_state = storage.getStateById("stopped");
		State running_state = storage.getStateById("running");
		
		assertEquals(ready_state, storage.getInitState());
		assertEquals(2, storage.getStateTransitions(running_state).size());
		assertEquals(ready_state, storage.getStateTransitions(stopped_state).iterator().next().getTarget());
	}
	
	@Test
	public void testGetValues()
	{
		HashMap<String, Integer> values = Parser.getValues("( ( x_0 10 ) ( y_0 99 ) )");
		assertEquals(10, (int) values.get("x"));
		assertEquals(99, (int) values.get("y"));
		assertEquals(2, values.size());
	}
}
