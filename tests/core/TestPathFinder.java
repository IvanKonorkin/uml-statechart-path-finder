/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import parser.Parser;
import storage.EdgeList;
import storage.Storage;

public class TestPathFinder {
	@Test
	public void testPathFinderWithoutCycles()
	{
		Parser parser = new Parser("tests/core/files/testPathFinderWithoutCycles.scxml");
		Storage storage = new EdgeList();
		parser.parseDocument(storage);
		PathFinder pf = new PathFinder(storage);
		State init_state = storage.getInitState();
		State target_state = storage.getStateById("5");
		ArrayList< ArrayList<Transition> > paths = pf.pathsToStateWithoutCycles(target_state);
		
		assertEquals(3, paths.size());
		for (ArrayList<Transition> path : paths) {
			State cur_state = init_state;
			for (int i = 0; i < path.size(); ++i) {
				Transition transition = path.get(i);
				assertTrue(storage.getStateTransitions(cur_state).contains(transition));
				cur_state = transition.getTarget();
			}
			assertEquals(target_state, cur_state);
		}
	}
	
	@Test
	public void testGetValuesForBadPath()
	{
		Parser parser = new Parser("tests/core/files/testGetValuesForBadPath.scxml");
		Storage storage = new EdgeList();
		parser.parseDocument(storage);
		PathFinder pf = new PathFinder(storage);
		State target_state = storage.getStateById("3");
		ArrayList< ArrayList<Transition> > path = pf.pathsToStateWithoutCycles(target_state);
		HashMap<String, Integer> values = pf.getValuesForPath(path.get(0));
		assertNull(values);
	}
	
	@Test
	public void testGetValuesForGoodPath()
	{
		Parser parser = new Parser("tests/core/files/testGetValuesForGoodPath.scxml");
		Storage storage = new EdgeList();
		parser.parseDocument(storage);
		PathFinder pf = new PathFinder(storage);
		State target_state = storage.getStateById("5");
		ArrayList< ArrayList<Transition> > path = pf.pathsToStateWithoutCycles(target_state);
		HashMap<String, Integer> values = pf.getValuesForPath(path.get(0));
		assertNotNull(values);
		int x = values.get("x");
		int y = values.get("y");
		int z = values.get("z");
		
		assertTrue(x < 10);
		assertEquals(40, y);
		assertTrue(z > 20);
	}
}
