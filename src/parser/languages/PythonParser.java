/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package parser.languages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.smtlib.ISolver;
import org.smtlib.ISort;
import org.smtlib.command.C_declare_fun;
import org.smtlib.impl.SMTExpr.Symbol;
import org.smtlib.impl.Sort.Parameter;

import parser.LanguageParser;

public class PythonParser implements LanguageParser {

	@Override
	public void parseCondition(Scanner sc, HashMap<String, Integer> variables, ISolver solver) {
		String left_var = sc.next();
		String comp = sc.next();
		if (comp.equals("==")) comp = "=";
		String expression = parseExpression(sc, variables, solver);
		left_var = getVariable(variables, left_var, solver);
		solver.assertExpr(new Symbol("(" + comp + " " + left_var + " " + expression + ")"));
	}

	private String incVariable(HashMap<String, Integer> variables,String var, ISolver solver) {
		if (variables.containsKey(var)) {
			variables.put(var, variables.get(var) + 1);
		} else {
			variables.put(var, 0);
		}
		String new_var = var + "_" + variables.get(var);
		solver.declare_fun(new C_declare_fun(new Symbol(new_var), new ArrayList<ISort>(), new Parameter(new Symbol("Int"))));
		return new_var;
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}

	private String parseExpression(Scanner sc, HashMap<String, Integer> variables, ISolver solver) {
		String first = sc.next();
		if (!isInteger(first)) first = getVariable(variables, first, solver);
		String action = sc.next();
		if (action.equals("//") || action.equals("/")) action = "div";
		if (action.equals("%")) action = "mod";
		String second = sc.next();
		if (!isInteger(second)) second = getVariable(variables, second, solver);
		return "(" + action + " " + first + " " + second + ")";
	}

	private String getVariable(HashMap<String, Integer> variables, String var, ISolver solver) {
		if (!variables.containsKey(var)) {
			variables.put(var, 0);
			String new_var = var + "_" + variables.get(var);
			solver.declare_fun(new C_declare_fun(new Symbol(new_var), new ArrayList<ISort>(), new Parameter(new Symbol("Int"))));
		}
		return var + "_" + variables.get(var);
	}

	@Override
	public void parseAction(Scanner sc, HashMap<String, Integer> variables, ISolver solver) {
		while (sc.hasNext()) {
			String left_var = sc.next();
			String assign = sc.next();
			String expression = parseExpression(sc, variables, solver);
			left_var = incVariable(variables, left_var, solver);
			solver.assertExpr(new Symbol("(" + assign + " " + left_var + " " + expression + ")"));
		}
	}

}
