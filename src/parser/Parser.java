/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package parser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import core.State;
import core.Transition;

import storage.Storage;

public class Parser {
	private Document _dom;
			
	public Parser(String file_path)
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			_dom = db.parse(file_path);
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private void parseStates(Element document_elem, Storage storage)
	{
		NodeList state_nl =  document_elem.getElementsByTagName("state");
		NodeList final_nl =  document_elem.getElementsByTagName("final");
				
		for(int i = 0 ; i < state_nl.getLength(); i++) {
			Element el = (Element) state_nl.item(i);
			State state = new State(el.getAttribute("id"));
			storage.addState(state);
		}
		
		for(int i = 0 ; i < final_nl.getLength(); i++) {
			Element el = (Element) final_nl.item(i);
			State state = new State(el.getAttribute("id"));
			storage.addState(state);
		}
	}
	
	private void parseTransitions(Element document_elem, Storage storage)
	{
		NodeList nl =  document_elem.getElementsByTagName("transition");
		
		for(int i = 0 ; i < nl.getLength(); i++) {
			Element el = (Element) nl.item(i);
			State state = storage.getStateById( ((Element) el.getParentNode()).getAttribute("id") );
			Transition transition = new Transition(
					el.getAttribute("id"),
					storage.getStateById(el.getAttribute("target")),
					el.getAttribute("cond"),
					el.getTextContent());
			storage.addTransition(state, transition);
		}
	}
	
	public void parseDocument(Storage storage)
	{
		Element document_elem = _dom.getDocumentElement();

		parseStates(document_elem, storage);
		parseTransitions(document_elem, storage);
	}
	
	public static HashMap<String, Integer> getValues(String str)
	{
		str = str.replaceAll("[()]", "");
		HashMap<String, Integer> values = new HashMap<String, Integer>();
		Scanner sc = new Scanner(str);
		while (sc.hasNext()) {
			String var = sc.next();
			values.put(var.substring(0, var.length() - 2), sc.nextInt());
		}
		return values;
	}
}
