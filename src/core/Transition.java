/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package core;

public class Transition {
	private State _to;
	private String _condition, _action, _id;

	public Transition(String id, State to, String condition, String action)
	{
		_id			= id;
		_to			= to;
		_condition	= condition;
		_action		= action;
	}
	
	public State getTarget()
	{
		return _to;
	}
	
	public String getId()
	{
		return _id;
	}
	
	public String getCondition()
	{
		return _condition;
	}
	
	public String getAction()
	{
		return _action;
	}
}
