/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import org.smtlib.ISolver;
import org.smtlib.SMT;
import org.smtlib.impl.SMTExpr.Keyword;
import org.smtlib.impl.SMTExpr.Symbol;
import org.smtlib.solvers.Solver_z3_4_3;

import parser.LanguageParser;
import parser.Parser;
import parser.languages.PythonParser;

import storage.Storage;

public class PathFinder {
	private Storage _storage;
	HashMap<State, Mark> _marks;
	
	enum Mark {
		NOT_VISITED,
		VISITED
	}
	
	public PathFinder(Storage storage)
	{
		_storage = storage;
	}
	
	public ArrayList< ArrayList<Transition> > pathsToStateWithoutCycles(State state)
	{
		ArrayList< ArrayList<Transition> > paths = new ArrayList< ArrayList<Transition> >();
		_marks = new HashMap<State, Mark>();
		addPathsWithoutCycles(paths, new ArrayList<Transition>(), _storage.getInitState(), state);
		_marks = null;
		return paths;
	}

	@SuppressWarnings("unchecked")
	private void addPathsWithoutCycles(ArrayList<ArrayList<Transition>> paths, ArrayList<Transition> cur_path, State state, State target)
	{
		if (state == target) {
			paths.add((ArrayList<Transition>) cur_path.clone());
		} else {
			_marks.put(state, Mark.VISITED);
			Collection<Transition> transitions = _storage.getStateTransitions(state);
			for (Transition transition : transitions) {
				State transition_target = transition.getTarget();
				if (_marks.get(transition_target) != Mark.VISITED) {
					cur_path.add(transition);
					addPathsWithoutCycles(paths, cur_path, transition_target, target);
					cur_path.remove(cur_path.size() - 1);
				}
			}
			_marks.put(state, Mark.NOT_VISITED);
		}
	}
	
	public HashMap<String, Integer> getValuesForPath(ArrayList<Transition> path)
	{
		SMT smt = new SMT();
		String z3_path = smt.readProperties().getProperty("org.smtlib.solver_z3_4_3.exec");
		ISolver solver = new Solver_z3_4_3(smt.smtConfig, z3_path);
		solver.start();
		solver.set_option(new Keyword(":produce-models"), new Symbol("true"));
		solver.set_logic("QF_LIA", null);
		
		HashMap<String, Integer> variables = new HashMap<String, Integer>();
		LanguageParser lang_parser = new PythonParser();
		
		for (int i = 0; i < path.size(); ++i) {
			Transition transition = path.get(i);
			
			String condition = transition.getCondition();
			String action = transition.getAction();
			
			if (condition != null) lang_parser.parseCondition(new Scanner(condition), variables, solver);
			if (action != null) lang_parser.parseAction(new Scanner(action), variables, solver);
		}
		
		if (!solver.check_sat().toString().equals("sat")) {
			return null;
		}
		
		Set<String> variables_set = variables.keySet();
		String all_variables = "";
		for (String var : variables_set) {
			all_variables += var + "_0 ";
		}
		String val_from_solver = solver.get_value(new Symbol(all_variables)).toString();
		return Parser.getValues(val_from_solver);
	}
}
