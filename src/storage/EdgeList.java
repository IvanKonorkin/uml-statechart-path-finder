/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import core.State;
import core.Transition;

public class EdgeList implements Storage {
	HashMap<String, State> id_to_state = new HashMap<String, State>();
	HashMap<State, ArrayList<Transition> > states = new HashMap<State, ArrayList<Transition> >();
	State init_state = null;

	@Override
	public void addState(State state) {
		if (state != null) {
			id_to_state.put(state.getId(), state);
			states.put(state, new ArrayList<Transition>());
			if (init_state == null) init_state = state;
		}
	}

	@Override
	public void addTransition(State state, Transition transition) {
		states.get(state).add(transition);
	}

	@Override
	public void setInitState(State state) {
		init_state = state;
	}

	@Override
	public State getInitState() {
		return init_state;
	}

	@Override
	public Collection<Transition> getStateTransitions(State state) {
		return states.get(state);
	}

	@Override
	public State getStateById(String id) {
		return id_to_state.get(id);
	}

}
