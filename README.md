UML statechart path finder
=========

To install program download [z3 solver] and edit ``config/jsmtlib.properties`` file this way (paste path to solver on your computer):

``org.smtlib.solver_z3_4_3.exec="C:/z3-4.3.0-x86/bin/z3.exe"``

[z3 solver]: http://z3.codeplex.com/